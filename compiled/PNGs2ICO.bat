@echo off
REM esasier to edit and run from here

SET output=z.ico
SET inputdir=temp
SET mode=-png

@echo on

win10iconTools pngs2ico %output% "%inputdir%" %mode%

@echo off

IF %ERRORLEVEL% NEQ 0 (
	echo.
	echo There were errors: returned error code: %ERRORLEVEL%
	pause
)