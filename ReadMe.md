win10iconTools
==============

[...] +++

MS ICO specifiation:
--------------------

|**block**	|**offset**	|**offset**	|**lenght**	|**description**				|
|-----------|-----------|-----------|-----------|-------------------------------|
|main header|	0		| 			|	2		|Reserved=0						|
|			|	2		|			|	2		|Image type: 1(.ICO) 2(.CUR)	|
|			|	4		|			|	2		|Number of images in container	|
|image head1|	6		|	0		|	1		|Pixel width					|
|			|	7		|	1		|	1		|Pixel height					|
|			|	8		|	2		|	1		|Color palette size or 0		|
|			|	9		|	3		|	1		|Reserved=0						|
|			|	A 		|	4		|	2		|Color planes=0 or 1			|
|			|	C 		|	6		|	2		|Bits per Pixel 				|
|			|	E 		|	8		|	4		|Image raw size					|
|			|	12		|	C 		|	4		|Offset of imageblock from BOF 	|
|image head2|	16		|	0		|	1		|Pixel width					|
|	...		|	...		|	...		|	...		|...							|
|imageblock1|	... 	|	...		|	...		|all image data goes here: 		|
|			|	... 	|	...		|	...		|	pngs included in whole 		|
|			|	... 	|	...		|	...		|	bmps missing first 14 bytes	|

| Windows 10 icon name                            | display 96DPI | load 96DPI | display 120DPI | load 120DPI | display 144DPI | load 144DPI | display 168DPI | load 168DPI | 
|-------------------------------------------------|---------------|------------|----------------|-------------|----------------|-------------|----------------|-------------| 
| alt-tab                                         | 24            | 32         | 30             | 32          | 36             | 32          | 42             | 32          | 
| desktop large                                   | 96            | 0          | 120            | 0           | 144            | 0           | 168            | 0           | 
| desktop medium                                  | 48            | 48         | 60             | 60          | 72             | 72          | 84             | 84          | 
| desktop small                                   | 32            | 32         | 40             | 40          | 48             | 48          | 56             | 56          | 
| explorer content, inc00                         | 32            | 32         | 40             | 40          | 48             | 48          | 56             | 56          | 
| explorer extra large                            | 256           | 0          | 256            | 0           | 256            | 0           | 256            | 0           | 
| explorer large                                  | 96            | 0          | 120            | 0           | 144            | 0           | 168            | 0           | 
| explorer medium                                 | 48            | 48         | 60             | 60          | 72             | 72          | 84             | 84          | 
| explorer small:inc04, list:inc03, details:inc02 | 16            | 16         | 20             | 20          | 24             | 24          | 28             | 28          | 
| explorer tiles, inc01                           | 48            | 48         | 60             | 60          | 72             | 72          | 84             | 84          | 
| startmenu medium                                | 32            | 32         | 40             | 40          | 48             | 48          | 56             | 63          | 
| startmenu programs                              | 24            | 24         | 30             | 30          | 36             | 36          | 42             | 42          | 
| startmenu search                                | 32            | 60         | 40             | 60          | 48             | 60          | 56             | 0           | 
| startmenu tile small                            | 24            | 24         | 30             | 31          | 36             | 39          | 42             | 47          | 
| taskbar normal                                  | 24            | 32         | 30             | 40          | 36             | 48          | 42             | 56          | 
| taskbar small                                   | 16            | 16         | 20             | 20          | 24             | 24          | 28             | 28          | 
| window icon                                     | 16            | 16         | 20             | 16          | 24             | 16          | 28             | 16          | 
| desktop inc00                                   | 16            | 16         | 20             | 20          | 24             | 24          | 28             | 28          | 
| desktop inc01                                   | 18            | 32         | 23             | 40          | 27             | 48          | 32             | 56          | 
| desktop inc02                                   | 20            | 30         | 25             | 40          | 30             | 48          | 35             | 56          | 
| desktop inc03                                   | 22            | 32         | 28             | 40          | 33             | 48          | 39             | 56          | 
| desktop inc04                                   | 24            | 32         | 30             | 40          | 36             | 48          | 42             | 56          | 
| desktop inc05                                   | 27            | 32         | 34             | 40          | 41             | 48          | 47             | 56          | 
| desktop inc06                                   | 30            | 32         | 38             | 40          | 45             | 48          | 53             | 56          | 
| desktop inc07                                   | 33            | 48         | 41             | 60          | 50             | 72          | 58             | 84          | 
| desktop inc08                                   | 37            | 48         | 46             | 60          | 56             | 72          | 65             | 84          | 
| desktop inc09                                   | 41            | 48         | 51             | 60          | 62             | 72          | 72             | 84          | 
| desktop inc10                                   | 46            | 48         | 58             | 60          | 69             | 72          | 82             | 84          | 
| desktop inc11                                   | 51            | 0          | 64             | 0           | 77             | 0           | 89             | 0           | 
| desktop inc12                                   | 57            | 0          | 71             | 0           | 86             | 0           | 100            | 0           | 
| desktop inc13                                   | 63            | 0          | 79             | 0           | 95             | 0           | 110            | 0           | 
| desktop inc14                                   | 70            | 0          | 88             | 0           | 105            | 0           | 123            | 0           | 
| desktop inc15                                   | 78            | 0          | 98             | 0           | 117            | 0           | 137            | 0           | 
| desktop inc16                                   | 87            | 0          | 109            | 0           | 131            | 0           | 152            | 0           | 
| desktop inc17                                   | 97            | 0          | 121            | 0           | 146            | 0           | 170            | 0           | 
| desktop inc18                                   | 108           | 0          | 135            | 0           | 162            | 0           | 189            | 0           | 
| desktop inc19                                   | 120           | 0          | 150            | 0           | 180            | 0           | 210            | 0           | 
| desktop inc20                                   | 133           | 0          | 166            | 0           | 200            | 0           | 233            | 0           | 
| desktop inc21                                   | 148           | 0          | 185            | 0           | 222            | 0           | 256            | 0           | 
| desktop inc22                                   | 164           | 0          | 205            | 0           | 246            | 0           | 256            | 0           | 
| desktop inc23                                   | 182           | 0          | 228            | 0           | 256            | 0           | 256            | 0           | 
| desktop inc24                                   | 202           | 0          | 253            | 0           | 256            | 0           | 256            | 0           | 
| desktop inc25                                   | 224           | 0          | 256            | 0           | 256            | 0           | 256            | 0           | 
| desktop inc26                                   | 249           | 0          | 256            | 0           | 256            | 0           | 256            | 0           | 
| explorer inc05                                  | 18            | 32         | 23             |             |                |             |                |             | 
| explorer inc06                                  | 20            | 32         | 25             |             |                |             |                |             | 
| explorer inc07                                  | 22            | 32         | 28             |             |                |             |                |             | 
| explorer inc08                                  | 23            | 32         | 29             |             |                |             |                |             | 
| explorer inc09                                  | 25            | 32         | 31             |             |                |             |                |             | 
| explorer inc10                                  | 27            | 32         | 34             |             |                |             |                |             | 
| explorer inc11                                  | 29            | 32         | 36             |             |                |             |                |             | 
| explorer inc12                                  | 31            | 32         | 39             |             |                |             |                |             | 
| explorer inc13                                  | 33            | 48         | 41             |             |                |             |                |             | 
| explorer inc14                                  | 35            | 48         | 44             |             |                |             |                |             | 
| explorer inc15                                  | 38            | 48         | 48             |             |                |             |                |             | 
| explorer inc16                                  | 41            | 48         | 51             |             |                |             |                |             | 
| explorer inc17                                  | 44            | 48         | 55             |             |                |             |                |             | 
| explorer inc18                                  | 47            | 48         | 59             |             |                |             |                |             | 
| explorer inc19                                  | 50            | 0          | 63             |             |                |             |                |             | 
| explorer inc20                                  | 54            | 0          | 68             |             |                |             |                |             | 
| explorer inc44                                  | 239           | 0          | 256            | 0           |                |             |                |             | 
| explorer inc45                                  | 256           | 0          | 256            | 0           |                |             |                |             | 