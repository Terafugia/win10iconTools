﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;

using System.IO;


namespace win10iconTools
{
    public class Ico
    {
        public static void SaveResized(int resX, int resY, string outfile, string infile, string interpolation, string smoothing)
        {
            Image SourceImage = Image.FromFile(infile);

            var DestRect = new Rectangle(0, 0, resX, resY);
            var DestImage = new Bitmap(resX, resY);

            DestImage.SetResolution(SourceImage.HorizontalResolution, SourceImage.VerticalResolution);

            using (var graphics = Graphics.FromImage(DestImage))
            {

                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;


                if (interpolation== "nearest")
                {
                    graphics.InterpolationMode = InterpolationMode.NearestNeighbor;
                }
                else if (interpolation == "bilinear")
                {
                    graphics.InterpolationMode = InterpolationMode.HighQualityBilinear;
                }
                else if (interpolation == "bicubic")
                {
                    graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                }
                else
                {

                }

                if (smoothing == "antialias")
                {
                    graphics.SmoothingMode = SmoothingMode.HighQuality;
                }
                else if (smoothing == "none")
                {
                    graphics.SmoothingMode = SmoothingMode.None;
                }
                else
                {

                }

                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;
                                               

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(SourceImage, DestRect, 0, 0, SourceImage.Width, SourceImage.Height, GraphicsUnit.Pixel, wrapMode);
                }

                //drawing.Save();

                DestImage.Save("temp\\" + resX + ".png", ImageFormat.Png);
                Console.Write(resX + "px ");

            }

        }

        public static Bitmap ResizeImage(Image image, int width, int height, string mode)
        {
            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {

                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                //graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.InterpolationMode = InterpolationMode.NearestNeighbor;
                graphics.SmoothingMode = SmoothingMode.None;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }

            return destImage;
        }

        public static int FindFontSize(int resX, int resY, string text, string fontname)
        {

            Image tempimage = new Bitmap(Convert.ToInt32(resX), Convert.ToInt32(resY), PixelFormat.Format32bppArgb);
            Graphics tempdraw = Graphics.FromImage(tempimage);

            for (int AdjustedSize = 3000 ; AdjustedSize >= 4 ; AdjustedSize--)
            {
                Font TestFont = new Font(fontname, AdjustedSize);

                SizeF AdjustedSizeNew = tempdraw.MeasureString(text, TestFont);

                if (Convert.ToInt32(resX) > Convert.ToInt32(AdjustedSizeNew.Width))
                {
                    // Good font, return it
                    return AdjustedSize;
                }
            }

            return 4;
            
        }

        

        public static void SaveNumberImage(int resX, int resY, string outfile, string backColor, string fontColor, string fontName)
        {
            Image img = new Bitmap(Convert.ToInt32(resX), Convert.ToInt32(resY), PixelFormat.Format32bppArgb);

            Graphics drawing = Graphics.FromImage(img);

            if (backColor.StartsWith("#"))
            {
                drawing.Clear(ColorTranslator.FromHtml(backColor));
            }
            else if (backColor=="none")
            {



            }
            else
            {
                drawing.Clear(Color.FromName(backColor));
            }
                
                     
            
            
            int fontsize = FindFontSize(resX, resY, resX.ToString(), fontName);

            Brush textBrush = new SolidBrush(ColorTranslator.FromHtml(fontColor));


      

            StringFormat sformat = new StringFormat();
            sformat.LineAlignment = StringAlignment.Center;
            sformat.Alignment = StringAlignment.Center;

            

            drawing.DrawString(resX.ToString(), new Font(fontName, fontsize), textBrush, resX/2,resY/2, sformat);

            //drawing.InterpolationMode = InterpolationMode.NearestNeighbor;
            //drawing.SmoothingMode = SmoothingMode.None;

            

            drawing.Save();

            

            img.Save("temp\\" + resX + ".png", ImageFormat.Png);
            Console.Write(resX + "px ");

            /*textBrush.Dispose();
            drawing.Dispose();
            drawing.Dispose();
            theFont.Dispose();
            actFont.Dispose();
            img.Dispose();*/
        }

        public static void Write(string outputfile,bool strict, string[] arr)
        {

            //byte[] test1 = { 0x03, 0x67, 0xFF };
            //byte[] test2 = { 0x44, 0x16, 0x4A };

            //ByteArr.Prepend(ref test1, ref test2);
            //ByteArr.Append(ref test1, ref test2);
            //ByteArr.CutEnd(ref test1, 2);
            //ByteArr.CutStart(ref test1, 2);

            int[] filesizes= new int[arr.Length];

            int counter = 0;
            

            int totalimagesize = 0;

            byte[] allimagedata = { };
            byte[] imageheaders = { };

            foreach (string image in arr)
            {
                int headResX = 0;
                int headResY = 0;
                
                byte[] imagedata = File.ReadAllBytes(image);
                filesizes[counter] = imagedata.Length;
                counter++;

                var imagestream = new MemoryStream(imagedata);
                var img = Image.FromStream(imagestream,false,false);
                
                if (img.Width <= 255)
                {
                    headResX = img.Width;
                }
                if (img.Height <= 255)
                {
                    headResY = img.Height;
                }

                byte[] headImgBlocksFront = { (byte)headResX, (byte)headResY, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0 };//8

                if (strict == true)
                {
                    int bpp = 0;
                    //create headless bmp
                    if (img.Width <= 255 && img.Height <= 255)
                    {
                        
                        using (var origImageStream = new MemoryStream(imagedata))
                        using (var bmpImageStream = new MemoryStream())
                        {
                            var bmpImage = Image.FromStream(origImageStream);
                            bmpImage.Save(bmpImageStream, ImageFormat.Bmp);

                            byte[] temp0 = bmpImageStream.ToArray();
                            ByteArr.Replace(ref imagedata, ref temp0);

                            // cut bmp's first 14bytes
                            ByteArr.CutStart(ref imagedata, 14);
                            bpp=Image.GetPixelFormatSize(bmpImage.PixelFormat);
                            bmpImage.Dispose();
                        }
                    }

                    headImgBlocksFront[6] = (byte)bpp;
                    headImgBlocksFront[7] = 0x0;
                }
            
                //header img block 16 bytes
                
                byte[] headImgBlockSize = BitConverter.GetBytes((uint)imagedata.Length); //4
                byte[] headImgBlockOffset = BitConverter.GetBytes((uint)((6 + arr.Length * 16 + totalimagesize) )); //4

                ByteArr.Append(ref imageheaders, ref headImgBlocksFront);
                ByteArr.Append(ref imageheaders, ref headImgBlockSize);
                ByteArr.Append(ref imageheaders, ref headImgBlockOffset);

                totalimagesize = totalimagesize + imagedata.Length;

                ByteArr.Append(ref allimagedata, ref imagedata);

               
            }
                       
                                // main header 6 bytes
            byte[] IcoData =  {0x0,0x0,0x1,0x0};
            byte[] imgpieces = BitConverter.GetBytes((ushort)counter); //2

            ByteArr.Append(ref IcoData, ref imgpieces);

            ByteArr.Append(ref IcoData, ref imageheaders);
            ByteArr.Append(ref IcoData, ref allimagedata);
                        
            File.WriteAllBytes(outputfile, IcoData);
            
            Console.WriteLine("\n\n" + outputfile + " written.");
        }
    }
}