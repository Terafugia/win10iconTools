﻿using System;

namespace win10iconTools
{
    public class ByteArr
    {
        public static void Append(ref byte[] destArr, ref byte[] sourceArr)
        {
            Array.Resize(ref destArr, destArr.Length + sourceArr.Length);
            Buffer.BlockCopy(sourceArr, 0, destArr, destArr.Length - sourceArr.Length, sourceArr.Length);
        }

        public static void Prepend(ref byte[] destArr, ref byte[] sourceArr)
        {
            byte[] tempArr = { };
            Array.Resize(ref tempArr, destArr.Length + sourceArr.Length);
            Buffer.BlockCopy(sourceArr, 0, tempArr, 0, sourceArr.Length);
            Buffer.BlockCopy(destArr, 0, tempArr, sourceArr.Length, destArr.Length);
            Array.Resize(ref destArr, tempArr.Length);
            Buffer.BlockCopy(tempArr, 0, destArr, 0, tempArr.Length);
        }

        public static void CutStart(ref byte[] Arr, int CutSize)
        {
            byte[] tempArr = { };
            Array.Resize(ref tempArr, Arr.Length - CutSize);
            Buffer.BlockCopy(Arr, CutSize, tempArr, 0, tempArr.Length);

            Array.Resize(ref Arr, tempArr.Length);
            Buffer.BlockCopy(tempArr, 0, Arr, 0, tempArr.Length);
        }

        public static void CutEnd(ref byte[] Arr, int CutSize)
        {
            Array.Resize(ref Arr, Arr.Length - CutSize);
        }

        public static void Replace(ref byte[] destArr, ref byte[] sourceArr)
        {
            Array.Resize(ref destArr, sourceArr.Length);
            Buffer.BlockCopy(sourceArr, 0, destArr, 0, sourceArr.Length);
        }
    }
}
