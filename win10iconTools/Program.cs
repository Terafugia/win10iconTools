﻿using System;
using System.IO;



namespace win10iconTools
{
    class Program
    {


        static void ShowHelp()
        {
           

            Console.WriteLine("  \nUsage: win10iconTools command options [mode]");
            Console.WriteLine("  mode: -strict or -png. Strict makes the icons under 256px in BMP format as MS specified.\n");

            Console.WriteLine("  Possible options:\n");
            Console.WriteLine("  makesizeicons: generate pngs with the size written on them and create an ico file");
            Console.WriteLine("          usage: win10iconTools makesizeicons output.ico background fontcolor fontname dimensionsByComma -mode");
            Console.WriteLine("             eg: win10iconTools makesizeicons output.ico none #ff55ff arial 16,32,64,128,256 -png");
            Console.WriteLine("             eg: win10iconTools makesizeicons output.ico none #ff55ff arial 16,32,64,128.256 -png");
            Console.WriteLine("           The period between numbers mean every number between them. \n");

            Console.WriteLine("       pngs2ico: generate ico from pngs");
            Console.WriteLine("          usage: win10iconTools pngs2ico output.ico input\\directory -mode");
            Console.WriteLine("             eg: win10iconTools pngs2ico output.ico input\\directory -strict\n");

            Console.WriteLine("   pngscale2ico: generate ico from one png by resizing");
            Console.WriteLine("          usage: win10iconTools pngscale2ico output.ico input.png interpolation smoothing dimensionsByComma -mode");
            Console.WriteLine("             eg: win10iconTools pngscale2ico output.ico input.png nearest none 16,32,64,128,256 -png");
            Console.WriteLine("  Interpolation: nearest(nearest neighbor), bilinear, bicubic");
            Console.WriteLine("      Smoothing: antialias, none\n");


            Console.WriteLine("");
        }

        static void Main(string[] args)
        {
            Console.WriteLine("\n*** Windows 10 icon tools by z4k*** \n");

            if (args.Length == 0)
            {
                ShowHelp();
                Console.Write("Error: no paramters given.\n\n[this pause is for checking alt-tab for example]\nPress any key to exit.");
                Console.ReadKey();
                Environment.Exit(0);
            }

            else if (args[0] == "makesizeicons")
            {
                if (args.Length != 7)
                {
                    ShowHelp();
                    Console.WriteLine("Error: makesizeicons: wrong parameter count");
                    Environment.Exit(87);

                }
                if (!args[1].EndsWith(".ico"))
                {
                    ShowHelp();
                    Console.WriteLine("Error: the output extension must be .ico");
                    Environment.Exit(87);
                }
                Directory.CreateDirectory("temp");
                if (!Directory.Exists("temp"))
                {
                    Console.WriteLine("Error: cannot create temp dir ");
                    Environment.Exit(3);
                }

                string[] sizesBase = args[5].Split(',');

                string[] sizes = { };

                foreach (string size0 in sizesBase)
                {
                    if (size0.Contains("."))
                    {
                        string[] interval = size0.Split('.');

                        int startnum = Convert.ToInt32(interval[0]);
                        int endnum = Convert.ToInt32(interval[1]);

                        for (int i0 = startnum ; i0 <= endnum ; i0++)
                        {
                            Array.Resize(ref sizes, sizes.Length + 1);
                            sizes[sizes.Length - 1] = i0.ToString();
                        }
                    }
                    else
                    {
                        Array.Resize(ref sizes, sizes.Length + 1);
                        sizes[sizes.Length - 1] = size0;
                    }
                }





                string[] filelist = new string[sizes.Length];
                int count = 0;

                foreach (string size in sizes)
                {



                    int dim0 = 0;

                    if (!Int32.TryParse(size, out dim0))
                    {
                        Console.WriteLine("Error: makesizeicons: invalid dimension: " + size);
                        Environment.Exit(87);
                    }

                    //int resX, int resY, string outfile, string backColor, string fontColor, string fontName)
                    Ico.SaveNumberImage(dim0, dim0, "temp\\" + size + ".png", args[2], args[3], args[4]);

                    filelist[count] = "temp\\" + size + ".png";
                    count++;

                }



                if (args[6] == "-strict")
                {
                    Ico.Write(args[1], true, filelist);
                }
                else if (args[6] == "-png")
                {
                    Ico.Write(args[1], false, filelist);
                }
                else
                {
                    Console.WriteLine("Error: makesizeicons: invalid mode: " + args[6]);
                    Environment.Exit(87);
                }

            }

            else if (args[0] == "pngs2ico")
            {
                if (args.Length != 4)
                {
                    ShowHelp();
                    Console.WriteLine("Error: pngs2ico: wrong parameter count");
                    Environment.Exit(87);
                }

                if (!args[1].EndsWith(".ico"))
                {
                    ShowHelp();
                    Console.WriteLine("Error: the output extension must be .ico");
                    Environment.Exit(87);
                }

                if (!Directory.Exists(args[2]))
                {
                    Console.WriteLine("Error: The directory you specified does not exist:\n" + args[2]);
                    Environment.Exit(3);
                }
                string[] inputfilelist = Directory.GetFiles(args[2], "*.png", SearchOption.TopDirectoryOnly);

                if (inputfilelist.Length == 0)
                {
                    Console.WriteLine("Error: There are no png files present in the directory:\n" + args[2]);
                    Environment.Exit(2);
                }

                if (args[3] == "-strict")
                {
                    Ico.Write(args[1], true, inputfilelist);
                }
                else if (args[3] == "-png")
                {
                    Ico.Write(args[1], false, inputfilelist);
                }
                else
                {
                    Console.WriteLine("Error: pngs2ico: invalid mode: " + args[3]);
                    Environment.Exit(87);
                }

            }
            else if (args[0] == "pngscale2ico")
            {

                //pngscale2ico output.ico input.png nearest none 16,32 -png











                if (args.Length != 7)
                {
                    ShowHelp();
                    Console.WriteLine("Error: pngscale2ico: wrong parameter count");
                    Environment.Exit(87);

                }
                if (!args[1].EndsWith(".ico"))
                {
                    ShowHelp();
                    Console.WriteLine("Error: the output extension must be .ico");
                    Environment.Exit(87);
                }
                Directory.CreateDirectory("temp");
                if (!Directory.Exists("temp"))
                {
                    Console.WriteLine("Error: cannot create temp dir ");
                    Environment.Exit(3);
                }
                if (!File.Exists(args[2]))
                {
                    Console.WriteLine("Error: The file you specified does not exist:\n" + args[2]);
                    Environment.Exit(3);
                }

                string[] sizesBase = args[5].Split(',');

                string[] sizes = { };

                foreach (string size0 in sizesBase)
                {
                    if (size0.Contains("."))
                    {
                        string[] interval = size0.Split('.');

                        int startnum = Convert.ToInt32(interval[0]);
                        int endnum = Convert.ToInt32(interval[1]);

                        for (int i0 = startnum ; i0 <= endnum ; i0++)
                        {
                            Array.Resize(ref sizes, sizes.Length + 1);
                            sizes[sizes.Length - 1] = i0.ToString();
                        }
                    }
                    else
                    {
                        Array.Resize(ref sizes, sizes.Length + 1);
                        sizes[sizes.Length - 1] = size0;
                    }
                }
                
                string[] filelist = new string[sizes.Length];
                int count = 0;

                foreach (string size in sizes)
                {

                    
                    int dim0 = 0;

                    if (!Int32.TryParse(size, out dim0))
                    {
                        Console.WriteLine("Error: pngscale2ico: invalid dimension: " + size);
                        Environment.Exit(87);
                    }
                    //  0               1       2           3     4    5     6
                    //pngscale2ico output.ico input.png nearest none  16,32 -png

                    //              resx,resy,out,in,interpolation,smoothing
                    Ico.SaveResized(dim0, dim0, args[1], args[2], args[3], args[4]);
                    
                    //int resX, int resY, string outfile, string backColor, string fontColor, string fontName)
                    //Ico.SaveNumberImage(dim0, dim0, "temp\\" + size + ".png", args[2], args[3], args[4]);

                    filelist[count] = "temp\\" + size + ".png";
                    count++;

                }



                if (args[6] == "-strict")
                {
                    Ico.Write(args[1], true, filelist);
                }
                else if (args[6] == "-png")
                {
                    Ico.Write(args[1], false, filelist);
                }
                else
                {
                    Console.WriteLine("Error: pngscale2ico: invalid mode: " + args[6]);
                    Environment.Exit(87);
                }












            }
            else
            {
                ShowHelp();
                Console.WriteLine("Error: unknown command: " + args[0]);
                Environment.Exit(87);
            }

            //Console.ReadKey();
        }
    }
}
